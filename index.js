const menu = document.querySelector('.menu');
const mobileMenu = document.querySelector('.mobile');
const closeMenu = document.querySelector('.close-menu');
menu.addEventListener('click', () => {
    console.log('click');
    mobileMenu.style.display = "block";
    menu.style.display = "none";
    closeMenu.style.display = "block";
});

closeMenu.addEventListener('click', () => {
    mobileMenu.style.display = "none";
    closeMenu.style.display = "none";
    menu.style.display = "block";
})